'use strict';

var electron = require('electron'),
    crypto = require('crypto'),
    path = require('path'),    
    fs = require('fs'),    
    request = require('./request.js'),
    tmp = require('./tmp.js'),
    elec = {
        app: electron.app,
        browserWindow: electron.BrowserWindow,
        webContents: electron.webContents,
        ipcMain: electron.ipcMain,
        dialog: electron.dialog,
        tray: electron.Tray,
        nativeImage: electron.nativeImage,
        menu: electron.Menu,
        globalShortcut: electron.globalShortcut,
        protocol: electron.protocol
    },
    view;

elec.app.setAboutPanelOptions({
    applicationName: 'Mozscape',
    applicationVersion: '1.0.0'
});

elec.app.on('ready', function(event) {
    view = new elec.browserWindow({
        width: 500,
        height: 430,
        show: false,
        frame: false,
        resizable: false,
        transparent: false
    });
    view.loadURL('file://' + path.join(__dirname, '../dist/index.html'));
    view.once('ready-to-show', function(event) {
        view.show();
        event.sender.send('html-insert', ['header','form','footer'], path.join(__dirname, '../dist/views/'), 'html');
    });

    var template = [{
        label: "Application",
        submenu: [{
            label: "About Application",
            selector: "orderFrontStandardAboutPanel:"
        }, {
            type: "separator"
        }, {
            label: "Quit",
            accelerator: "Command+Q",
            click: function() {
                elec.app.quit();
            }
        }]
    }, {
        label: "Edit",
        submenu: [{
            label: "Undo",
            accelerator: "CmdOrCtrl+Z",
            selector: "undo:"
        }, {
            label: "Redo",
            accelerator: "Shift+CmdOrCtrl+Z",
            selector: "redo:"
        }, {
            type: "separator"
        }, {
            label: "Cut",
            accelerator: "CmdOrCtrl+X",
            selector: "cut:"
        }, {
            label: "Copy",
            accelerator: "CmdOrCtrl+C",
            selector: "copy:"
        }, {
            label: "Paste",
            accelerator: "CmdOrCtrl+V",
            selector: "paste:"
        }, {
            label: "Select All",
            accelerator: "CmdOrCtrl+A",
            selector: "selectAll:"
        }]
    }];
    elec.menu.setApplicationMenu(elec.menu.buildFromTemplate(template));

});

elec.app.on('window-all-closed', () => {
    if (process.platform != 'darwin') {
        elec.app.quit();
    }
});

elec.ipcMain.on('data-request', function(event, data, cols, keys) {
    var post = request.post(data, cols, keys).then(function(resp) {
        event.sender.send('data-response', resp, data, cols);
    }).catch(function(err) {
        event.sender.send('error-response', err, data, cols);
    });
});