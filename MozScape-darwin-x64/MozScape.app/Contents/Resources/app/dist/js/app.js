var electron = require('electron').remote,
    ipcRenderer = require('electron').ipcRenderer,
    BrowserWindow = electron.BrowserWindow,
    fs = require('fs'),
    moz = {
        utils: {
            json2csv: require('./utils/json2csv.js')
        },
        views: {
            form: require('./views/form.js')
        },
        models: {}
    };

ipcRenderer.on('data-response', function(event, respData, reqData, columns) {
    var obj = [],
        val;
    reqData.forEach(function(element, index, arr) {
        val = this[index];
        obj.push({
            address: element,
            value: val[Object.keys(val)[0]]
        })
    }, JSON.parse(respData));
    var savePath = electron.dialog.showSaveDialog({
        defaultPath: 'result-' + columns + '.csv'
    });
    fs.writeFile(savePath, moz.utils.json2csv(obj), function(err) {
        console.log(arguments);
        document.getElementsByClassName('form')[0].reset();
    });
});

ipcRenderer.on('html-insert', function(event, els, base, ext) {
    $('#render').html('');
    els.forEach(function(element) {
        setTimeout(function() {
            fs.readFile(base + element + '.' + ext, 'utf8', function(err, html) {
                $('#render').append(html);
                if (typeof moz.views[element] !== 'undefined') {
                    new moz.views[element]({}, document.getElementsByClassName(element)[0], ipcRenderer)
                }
            });
        }, 0);
    });
});