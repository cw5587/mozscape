var fs = require("fs"),
    tmp = require("tmp");
module.exports = {
    saveFile: function(obj, str) {
        var _this = this,
        	str = str || '',
            tmpobj = tmp.fileSync(obj);
        return new Promise(function(resolve, reject) {
            fs.appendFile(tmpobj.name, str, function(err) {
	            fs.readFile(tmpobj.name, function read(err, data) {
	                if (err) {
	                    return reject(err);
	                }
	                return resolve(data);
	            });
            });
            tmp.setGracefulCleanup();
        });
    }
};