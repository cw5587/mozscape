var crypto = require('crypto'),
    http = require('http');
    
module.exports = {
    isArray: function(data) {
        
        var ret = true;
        
        try {
            Object.prototype.toString.call(data) === "[object Array]";
        } catch (e) {
            ret = false;
        }
        
        return ret;

    },
    sign: function(keys, expires) {
    
        var stringToSign = keys.accessId + "\n" + expires,
            signature = crypto.createHmac('sha1', keys.secretKey).update(stringToSign).digest('base64');
        signature = encodeURIComponent(signature);
    
        return signature;
    
    },
    post: function(data, cols, keys) {
        
        var data = this.isArray(data) ? JSON.stringify(data) : '[]',
            expires = Math.floor((Date.now() / 1000)) + 300,
            signature = this.sign(keys, expires),
            options = {
                hostname: 'lsapi.seomoz.com',
                path: '/linkscape/url-metrics/?Cols=' + cols + '&AccessID=' + keys.accessId + '&Expires=' + expires + '&Signature=' + signature,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': data.length
                }
            };

        return new Promise(function(resolve, reject) {
            var request = http.request(options, function(response) {
                var body = "";
                response.on('data', function(chunk) {
                    return body += chunk;
                });
                response.on('end', function() {
                    return resolve(body);
                });
            });
            request.on('error', function(err) {
                return reject(err);
            });
            request.write(data);
            request.end();
        });

    }
};