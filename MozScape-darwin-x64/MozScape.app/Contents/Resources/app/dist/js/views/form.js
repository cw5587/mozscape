var formView = function(model, element, ipcRenderer) {
    this.model = model;
    this.element = element;
    this.ipcRenderer = ipcRenderer;
    this.arr = [];
    this.init();
};
formView.prototype = {
    init: function() {
        var _this = this;
        this.element.addEventListener('submit', function(e) {
            e.preventDefault();
            _this.submit(e);
        });
        document.getElementsByName('fileInput')[0].addEventListener('change', function(e) {
            _this.upload(e);
        });
    },
    browserSupportFileUpload: function() {
        var isCompatible = false;
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            isCompatible = true;
        }
        return isCompatible;
    },
    upload: function(e) {
        var _this = this;

        if (!this.browserSupportFileUpload()) {
            
            alert('The File APIs are not fully supported in this browser!');
        
        } else {
            var data = null,
            	file = e.target.files[0],
            	reader = new FileReader();
            
            reader.readAsText(file);
            
            reader.onload = function(event) {
                var csvData = event.target.result;
                _this.arr = csvData.split(',');
            };
            
            reader.onerror = function() {
                alert('Unable to read ' + file.fileName);
            };

        }
    },
    submit: function(e) {
        this.ipcRenderer.send('data-request', this.arr, document.getElementsByName('Cols')[0].value, {
            accessId: document.getElementsByName('accessId')[0].value,
            secretKey: document.getElementsByName('secretKey')[0].value
        });
    }
};
module.exports = formView;